package net.mcreator.the_ultimate_combat_uptate;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.SwordItem;
import net.minecraft.item.Item;
import net.minecraft.item.IItemTier;

@Elementsthe_ultimate_combat_uptate.ModElement.Tag
public class MCreatorRubydagger extends Elementsthe_ultimate_combat_uptate.ModElement {
	@ObjectHolder("the_ultimate_combat_uptate:rubydagger")
	public static final Item block = null;

	public MCreatorRubydagger(Elementsthe_ultimate_combat_uptate instance) {
		super(instance, 9);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new SwordItem(new IItemTier() {
			public int getMaxUses() {
				return 2000;
			}

			public float getEfficiency() {
				return 4f;
			}

			public float getAttackDamage() {
				return 3f;
			}

			public int getHarvestLevel() {
				return 1;
			}

			public int getEnchantability() {
				return 2;
			}

			public Ingredient getRepairMaterial() {
				return null;
			}
		}, 3, -1F, new Item.Properties().group(MCreatorMelle.tab)) {
		}.setRegistryName("rubydagger"));
	}
}
