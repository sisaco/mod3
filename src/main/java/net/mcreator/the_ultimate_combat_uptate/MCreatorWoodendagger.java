package net.mcreator.the_ultimate_combat_uptate;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.SwordItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Item;
import net.minecraft.item.IItemTier;
import net.minecraft.block.Blocks;

@Elementsthe_ultimate_combat_uptate.ModElement.Tag
public class MCreatorWoodendagger extends Elementsthe_ultimate_combat_uptate.ModElement {
	@ObjectHolder("the_ultimate_combat_uptate:woodendagger")
	public static final Item block = null;

	public MCreatorWoodendagger(Elementsthe_ultimate_combat_uptate instance) {
		super(instance, 1);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new SwordItem(new IItemTier() {
			public int getMaxUses() {
				return 60;
			}

			public float getEfficiency() {
				return 4f;
			}

			public float getAttackDamage() {
				return -2f;
			}

			public int getHarvestLevel() {
				return 1;
			}

			public int getEnchantability() {
				return 2;
			}

			public Ingredient getRepairMaterial() {
				return Ingredient.fromStacks(new ItemStack(Blocks.OAK_PLANKS, (int) (1)), new ItemStack(Blocks.SPRUCE_PLANKS, (int) (1)),
						new ItemStack(Blocks.BIRCH_PLANKS, (int) (1)), new ItemStack(Blocks.JUNGLE_PLANKS, (int) (1)), new ItemStack(
								Blocks.ACACIA_PLANKS, (int) (1)), new ItemStack(Blocks.DARK_OAK_PLANKS, (int) (1)));
			}
		}, 3, -1F, new Item.Properties().group(MCreatorMelle.tab)) {
		}.setRegistryName("woodendagger"));
	}
}
