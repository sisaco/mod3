package net.mcreator.the_ultimate_combat_uptate;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.Item;
import net.minecraft.block.BlockState;

@Elementsthe_ultimate_combat_uptate.ModElement.Tag
public class MCreatorRuby extends Elementsthe_ultimate_combat_uptate.ModElement {
	@ObjectHolder("the_ultimate_combat_uptate:ruby")
	public static final Item block = null;

	public MCreatorRuby(Elementsthe_ultimate_combat_uptate instance) {
		super(instance, 17);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new ItemCustom());
	}

	public static class ItemCustom extends Item {
		public ItemCustom() {
			super(new Item.Properties().group(ItemGroup.MATERIALS).maxStackSize(64));
			setRegistryName("ruby");
		}

		@Override
		public int getItemEnchantability() {
			return 0;
		}

		@Override
		public int getUseDuration(ItemStack itemstack) {
			return 0;
		}

		@Override
		public float getDestroySpeed(ItemStack par1ItemStack, BlockState par2Block) {
			return 1F;
		}
	}
}
